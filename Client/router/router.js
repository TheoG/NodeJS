var HomeController = require('./../controllers/HomeController');
var ResultatController = require('./../controllers/ResultatController');
var EcurieController = require('./../controllers/EcurieController');
var PiloteController = require('./../controllers/PiloteController');
var CircuitController = require('./../controllers/CircuitController');

// Routes
module.exports = function(app){

    // Main Routes
    app.get('/', HomeController.Index);

    // pilotes
    app.get('/repertoirePilote', PiloteController.Repertoire);
    app.get('/repertoirePilote/:lettre', PiloteController.DetailLettre);
    app.get('/repertoirePilote/pilote/:numero', PiloteController.DetailPilote);

    // circuits
    app.get('/circuits', CircuitController.ListerCircuit);
    app.get('/circuits/:num', CircuitController.DetailCircuit);

    // Ecuries
    app.get('/ecuries', EcurieController.ListerEcurie);
    app.get('/detailEcurie/:numero', EcurieController.DetailEcurie);

    //Résultats
    app.get('/resultats', ResultatController.ListerResultat);
    app.get('/detailResultat/:numero', ResultatController.DetailResultat);


    // tout le reste
    app.get('*', HomeController.Index);
    app.post('*', HomeController.Index);

};
