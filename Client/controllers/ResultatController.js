var model = require("../models/grandprix.js"),
    pilote = require("../models/pilote.js"),
    async = require("async");


// //////////////////////////L I S T E R    R E S U L T A T S
module.exports.ListerResultat = function(request, response){
    response.title = 'Liste des résulats des grands prix';

    model.getListeGrandPrix(function(err, result) {
        if (err) {
            console.log(err);
            return;
        }
        
        response.listeResultat = result;
        response.render('listerResultat', response);
    });
};

module.exports.DetailResultat = function(request, response) {
    response.title = "Détail d'un grand prix";
    
    var numero = request.params.numero;
    
    async.parallel([
        function(callback) {
            model.getListeGrandPrix(function(err, result) {callback(null, result)});
        },
        
        function(callback) {
            model.getDetailGrandPrix(numero, function(err, result) {callback(null, result)});
        },
        
        function(callback) {
            pilote.getClassementParGrandPrix(numero, function(err, result) {callback(null, result)});
        }
    ],
    
    function(err, result) {
        if (err) {
            console.log(err);
            return;
        }
        
        response.listeResultat = result[0];
        response.grandPrix = result[1][0];
        response.detailResultat = result[2];
    
        response.render('listerResultat', response);
    });  
};
