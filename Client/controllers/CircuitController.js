var model = require("../models/circuit.js"),
    async = require("async");
    
// ////////////////////// L I S T E R     C I R C U I T S

module.exports.ListerCircuit = function(request, response){
    response.title = 'Liste des circuits';

    model.getListeCircuit(function(err, result) {
        if (err) {
            console.log(err);
            return;
        }
        response.circuit = result;
        response.render('listerCircuit', response);
    });
};


module.exports.DetailCircuit = function(request, response){
    response.title = 'Liste des circuits';

    var numero = request.params.num;
    
    async.parallel([
        function(callback) {
            model.getListeCircuit(function(err, result) {callback(null, result)});
        },
        
        function(callback) {
            model.getDetailCircuit(numero, function(err, result) {callback(null, result)});
        }
        
    ],
    
    function(err, result) {
        if (err) {
            console.log(err);
            return;
        }
        
        
        response.circuit = result[0];
        response.details = result[1][0];
        response.pneu = result[2];
        
        result[1][0].CIRLONGUEUR = formaterNombre(result[1][0].CIRLONGUEUR);
        result[1][0].CIRNBSPECTATEURS = formaterNombre(result[1][0].CIRNBSPECTATEURS);
        
        response.render('listerCircuit', response);
    });
};


/*
* Fonction formattant un nombre en ajoutant des espaces entre les milliers / millions, etc.
* Entrée : Un nombre
* Sortie : Une string au bon format
*/
function formaterNombre(nbr) {
    nombre = ''+nbr;
    var count = 0;
    var retour = '';
    
    for (var i = nombre.length - 1; i >= 0; i--) {
        if(count !=0  && count % 3 == 0)
            retour = nombre[i]+' '+retour ;
         else
            retour = nombre[i]+retour ;
         count++;
    }
    return retour;
}