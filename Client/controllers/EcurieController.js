var model = require('../models/ecurie.js'),
    pneu = require("../models/pneu.js"),
    pilote = require("../models/pilote.js"),
    voiture = require("../models/voiture.js"),
    sponsor = require("../models/sponsor.js"),
    
    async = require('async');

// //////////////////////// L I S T E R  C I R C U I T S

/*
<!--
* listeEcurie contient par exemple :
* [
* { ecunum: 5, payadrdrap: 'AAA',ecunom:'rrr' },
* { ecunum: 6, payadrdrap: 'BAA' ,ecunom:'ggg'},
* { ecunum: 7, payadrdrap: 'ACA' ,ecunom:'kkkk'}
*  ]
*
* response.title est passé à main.handlebars via la vue ListerEcurie
* il sera inclus dans cette balise : <title> {{title}}</title>
*/

module.exports.ListerEcurie = function(request, response){
    response.title = 'Liste des écuries';

    model.getListeEcurie( function (err, result) {
        if (err) {
            // gestion de l'erreur
            console.log(err);
            return;
        }
        response.listeEcurie = result;
        response.render('listerEcurie', response);
    });
};


module.exports.DetailEcurie = function(request, response){
    response.title = 'Détail d\'une écurie';

    var numero = request.params.numero;
    
    async.parallel([
        function(callback) {
            model.getListeEcurie(function(err, result) {callback(null, result)});
        },
        
        function(callback) {
            model.getDetailEcurie(numero, function(err, result) {callback(null, result)});
        },
        
        function(callback) {
            pneu.getFournPneuByEcurie(numero, function(err, result) {callback(null, result)});
        },
        
        function(callback) {
            pilote.getListePiloteParEcurie(numero, function(err, result) {callback(null, result)});
        },
        
        function(callback) {
            voiture.getListeVoitureEcurie(numero, function(err, result) {callback(null, result)});
        },
        
        function(callback) {
            sponsor.getSponsorNumEcurie(numero, function(err, result) {callback(null, result)});
        }
    ],
    
    function(err, result) {
        if (err) {
            console.log(err);
            return;
        }
            
        response.listeEcurie = result[0];
        response.ecurie = result[1][0];
        response.pneu = result[2][0];
        response.listePilote = result[3];
        response.listeVoiture = result[4];
        response.sponsor = result[5];
    
        response.render('listerEcurie', response);
    });
};

