var model = require("../models/grandprix.js");

// ////////////////////////////////////////////// A C C U E I L

module.exports.Index = function(request, response) {
    response.title = "Bienvenue sur le site de WROOM (IUT du Limousin).";

    model.getLastGrandPrix(function(err, result) {
        if (err) {
            console.log(err);
            return;
        }

        response.grandprix = result[0];
        response.render('home', response);
    });
};
