var model = require("../models/pilote.js"),
    sponsor = require("../models/sponsor.js"),
    photo = require("../models/photo.js");

var async = require("async");
// ///////////////////////// R E P E R T O I R E    D E S    P I L O T E S

module.exports.Repertoire = 	function(request, response) {
    response.title = 'Répertoire des pilotes';

    model.getPremiereLettre(function(err, result) {
        if (err) {
            console.log(err);
            return;
        }
        response.lettre = result;
        response.render('repertoirePilotes', response);
    });
};

module.exports.DetailLettre = 	function(request, response) {
    response.title = 'Détail des pilotes par lettre';
    var lettre = request.params.lettre;
    
    async.parallel([
        function(callback) {
            model.getPremiereLettre(function(err, result) {callback(null, result)});
        },
        
        function(callback) {
            model.getPiloteLettre(lettre, function(err2, result2) {callback(null, result2)});
        }
    ],

    function(err, result) {
        if (err) {
            console.log(err);
            return;
        }
    
        response.lettre = result[0];
        response.listePilote = result[1];
        
        response.render('repertoirePiloteLettre', response);
    });
};

module.exports.DetailPilote = 	function(request, response) {
    response.title = 'Détail des pilotes par lettre';
    
    var numero = request.params.numero;
    
    async.parallel([
        function(callback) {
            model.getPremiereLettre(function(err, result) {callback(null, result)});
        },
        
        function(callback) {
            model.getPiloteNumero(numero, function(err, result) {callback(null, result)});
        },
        
        function(callback) {
            model.getEcuriePilote(numero, function(err, result) {callback(null  , result)});
        },
        
        function(callback) {
            sponsor.getSponsorNumPilote(numero, function(err, result) {callback(null, result)});
        },
        
        function(callback) {
            photo.getPhotoPilote(numero, function(err, result) {callback(null, result)});
        },
        
        function(callback) {
            photo.getPhotoID(numero, function(err, result) {callback(null, result)});
        },
    ],

    function(err, result) {
        if (err) {
            console.log(err);
            return;
        }
                
        response.lettre = result[0];
        response.pilote = result[1][0];
        response.ecurie = result[2][0];
        response.sponsor = result[3];
        response.photos = result[4];
        response.photoID = result[5][0];
        
        response.render('repertoirePiloteDetail', response);
    });
};
