var db = require("../configDb");


module.exports.getListeCircuit = function(callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "SELECT CIRNOM, CIRNUM, PAYADRDRAP FROM circuit c JOIN pays p ON c.PAYNUM = p.PAYNUM ORDER BY CIRNOM";
            
            connexion.query(sql, callback);
            
            connexion.release();
        } 
    });
};

module.exports.getDetailCircuit = function(numero, callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "SELECT CIRNOM, CIRLONGUEUR, CIRNBSPECTATEURS, CIRTEXT, CIRADRESSEIMAGE, PAYNOM FROM circuit c JOIN pays p ON c.PAYNUM = p.PAYNUM WHERE CIRNUM = " + numero + " ORDER BY CIRNOM";

            connexion.query(sql, callback);
            
            connexion.release();
        } 
    });
};
