var db = require('../configDb');

module.exports.getFournPneuByEcurie = function(numero, callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "SELECT FPNOM, FPADRESSE, FPDATEFISA FROM fourn_pneu f JOIN ecurie e ON e.FPNUM = f.FPNUM WHERE e.ECUNUM = " + numero;
            
            connexion.query(sql, callback);
            
            connexion.release();
        }
    })
}