var db = require('../configDb');

module.exports.getSponsorNumPilote = function(number, callback) {

    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "SELECT SPONOM, SPOSECTACTIVITE FROM sponsor s JOIN sponsorise ss ON s.sponum = ss.sponum WHERE ss.pilnum = " + number;
            
            connexion.query(sql, callback);
            
            connexion.release();
        }
    });
};

module.exports.getSponsorNumEcurie = function(numero, callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "SELECT s.SPONUM, s.SPONOM, s.SPOSECTACTIVITE FROM sponsor s JOIN finance f ON s.SPONUM = f.SPONUM WHERE f.ECUNUM = " + numero;
                        
            connexion.query(sql, callback);
            
            connexion.release();
        }
    });
};