var db = require('../configDb');

module.exports.getPremiereLettre = function(callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "SELECT DISTINCT SUBSTRING(PILNOM, 1, 1) as lettre FROM pilote ORDER BY lettre";
            
            connexion.query(sql, callback);
            
            connexion.release();
        }
    });
};

module.exports.getPiloteLettre = function(lettre, callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "SELECT p.PILNUM, PILNOM, PILPRENOM, PHOADRESSE FROM pilote p JOIN photo ph ON p.PILNUM = ph.PILNUM WHERE PILNOM LIKE '" + lettre + "%' AND PHONUM = 1";
            
            connexion.query(sql, callback);
            
            connexion.release();
        }
    });
};

module.exports.getPiloteNumero = function(numero, callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "SELECT p.PILNUM, p.PAYNUM, p.PILNOM, p.PILPRENOM, p.PILDATENAIS, p.PILPOINTS, p.PILPOINTS, p.PILPOIDS, p.PILTAILLE, p.PILTEXTE, pa.PAYNAT FROM pilote p JOIN pays pa ON p.PAYNUM = pa.PAYNUM WHERE p.PILNUM = " + numero + " GROUP BY p.PILNUM";
            
            connexion.query(sql, callback);
            
            connexion.release();
        }
    });
};

module.exports.getEcuriePilote = function(numero, callback) {
    db.getConnection(function(err, connexion) {
        if(!err) {
            var sql = "SELECT ECUNOM FROM ecurie e JOIN pilote p ON p.ecunum = e.ecunum WHERE p.pilnum = " + numero;
            
            connexion.query(sql, callback);
            
            connexion.release();
        } 
    });
};

module.exports.getListePiloteParEcurie = function(numero, callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "SELECT pi.PILNUM, pi.PILNOM, pi.PILPRENOM, ph.PHOADRESSE FROM pilote pi JOIN photo ph ON ph.PILNUM = pi.PILNUM WHERE pi.ECUNUM = " + numero + " GROUP BY pi.PILNUM";
            
            connexion.query(sql, callback);
            
            connexion.release();
        }
    });
};

module.exports.getPiloteIdentite = function(numero, callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "SELECT PILNUM, PILNOM PILPRENOM WHERE PILNUM = " + numero;
            
            connexion.query(sql, callback);
            
            connexion.release();
        }
    });
};

module.exports.getClassementParGrandPrix = function(numero, callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "SELECT p.PILNUM, PILNOM, PILPRENOM, PILPOINTS, c.TEMPSCOURSE FROM pilote p JOIN course c ON c.PILNUM = p.PILNUM WHERE c.GPNUM = " + numero + " AND PILPOINTS IS NOT NULL ORDER BY c.TEMPSCOURSE";
            
            connexion.query(sql, callback);
            
            connexion.release();
        }      
    });
};





