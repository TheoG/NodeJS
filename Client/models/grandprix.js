var db = require("../configDb");

module.exports.getLastGrandPrix = function(callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "SELECT gpnum, gpdate, gpdatemaj FROM grandprix WHERE gpdatemaj >= ALL (SELECT gpdatemaj FROM grandprix)";

            connexion.query(sql, callback);

            connexion.release();
        } 
    });
};


module.exports.getListeGrandPrix = function(callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "SELECT g.GPNUM, g.GPNOM, pa.PAYADRDRAP FROM grandprix g JOIN circuit c ON g.CIRNUM = c.CIRNUM JOIN pays pa ON pa.PAYNUM = c.PAYNUM ORDER BY g.GPNOM";
            
            connexion.query(sql, callback);
            
            connexion.release();
        } 
    });
};

module.exports.getDetailGrandPrix = function(numero, callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "SELECT g.GPNUM, g.GPNOM, g.GPDATE, g.GPNBTOURS, g.GPDATEMAJ, g.GPCOMMENTAIRE FROM grandprix g WHERE g.GPNUM = " + numero;
            
            connexion.query(sql, callback);
            
            connexion.release();
        } 
    });
};
