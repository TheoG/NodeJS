var db = require('../configDb');

module.exports.getPhotoID = function(numero, callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            sql = "SELECT PHONUM, PHOSUJET, PHOCOMMENTAIRE, PHOADRESSE FROM photo WHERE PHONUM = 1 AND PILNUM = " + numero;
            
            connexion.query(sql, callback);
            
            connexion.release();
        } 
    });
};

module.exports.getPhotoPilote = function(numero, callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "SELECT PHONUM, PHOSUJET, PHOCOMMENTAIRE, PHOADRESSE FROM photo WHERE PHONUM != 1 AND PILNUM = " + numero;
            
            connexion.query(sql, callback);
            
            connexion.release();
        } 
    });  
};