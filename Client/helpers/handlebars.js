var moment = require('moment');

function hbsHelpers(handlebars) {
    return handlebars.create({
        defaultLayout: 'main',

        partialsDir: ['views/partials/'],

        helpers: {
            inc: function(value, options) {
                return parseInt(value) + 1;
            },

            formatDate: function (date, format) {
                return moment(date).format(format);
            },

            ifCond : function (v1, operator, v2, options) {

                switch (operator) {
                    case '==':
                        return (v1 == v2) ? options.fn(this) : options.inverse(this);
                    case '===':
                        return (v1 === v2) ? options.fn(this) : options.inverse(this);
                    case '<':
                        return (v1 < v2) ? options.fn(this) : options.inverse(this);
                    case '<=':
                        return (v1 <= v2) ? options.fn(this) : options.inverse(this);
                    case '>':
                        return (v1 > v2) ? options.fn(this) : options.inverse(this);
                    case '>=':
                        return (v1 >= v2) ? options.fn(this) : options.inverse(this);
                    case '&&':
                        return (v1 && v2) ? options.fn(this) : options.inverse(this);
                    case '||':
                        return (v1 || v2) ? options.fn(this) : options.inverse(this);
                    default:
                        return options.inverse(this);
                }
            }
        }
    });
}

module.exports = hbsHelpers;