var model = require("../models/grandprix.js"),
    pilote = require("../models/pilote.js"),
    ecurie = require("../models/ecurie.js"),
    
    async = require("async");


// //////////////////////////L I S T E R    R E S U L T A T S
module.exports.ListerResultat = function(request, response){
    response.title = 'Résultat des grands prix';
    
    model.getListeGrandPrix(function(err, result) {
        if (err) {
            console.log(err);
            return;
        }
        
        response.listeResultat = result;
        response.render('listerResultat', response);
    });
};

module.exports.SaisieResultat = function(request, response){
    response.title = 'Saisie des résultats';
    
    var numResultat;
    
    if (request.body.grandPrix) {
        request.session.numResultat = request.body.grandPrix;
        numResultat = request.body.grandPrix;
    } else {
        numResultat = request.session.numResultat;
    }
    
    async.parallel([
        function(callback) {
            model.getClassementParGrandPrix(numResultat, function(err, result) {callback(null, result)});
        },
        
        function(callback) {
            model.getPoints(function(err, result) {callback(null, result)});
        },
        
        function(callback) {
            pilote.getListePiloteAvecEcurie(numResultat, function(err, result) {callback(null, result)});
        }
    ],
    
    function(err, result) {
        if (err) {
            console.log(err);
            return;
        }

        response.resultats = result[0];
        response.points = result[1];
        response.pilotes = result[2];
        result[0] != null ? response.nbPilote = result[0].length : 0;
        response.nbPilote != 10 ? request.session.pointsPilote = response.points[response.nbPilote].PTNBPOINTSPLACE : 0;
        response.render('saisirResultat', response);
    });
};

module.exports.AjouterResultat = function(request, response) {
    var numero = request.session.numResultat;
    var nbPoints = request.session.pointsPilote;

    var heures      = testDix(request.body.heures);
    var minutes     = testDix(request.body.minutes);
    var secondes    = testDix(request.body.secondes);
    
    var heure = heures + ":" + minutes + ":" + secondes;
    
    
    var valeurs = {
        GPNUM:              numero,
        PILNUM:             request.body.pilote,
        TEMPSCOURSE:        heure  
    };

    async.parallel([
        function(callback) {
            model.ajouterResultat(valeurs, function(err, result) {callback(null, result)});
        },
        
        function(callback) {
            pilote.updatePoints(request.body.pilote, nbPoints, function(err, result) {callback(null, result)});
        },
        
        function(callback) {
            ecurie.updatePoints(numero, nbPoints, function(err, result) {callback(null, result)});
        }
    ],
                  
    function(err, result) {
        if (err) {
            console.log(err);
            return;
        } 
    });
    
    response.redirect('../saisie');    
};

module.exports.SupprimerResultat = function(request, response) {
    var numeroGP = request.session.numResultat;
    var numeroPil = request.params.numero;


    async.series([
        function(callback) {
            model.supprimerResultat(numeroPil, numeroGP, function(err, result) {callback(null, result)});
        },

        function(callback) {
            pilote.supprimerPoints(numeroPil, function(err, result) {callback(null, result)});
        }
    ],

    function(err, result) {
        if (err) {
            console.log(err);
            return;
        } 
    });

    response.redirect('../../saisie');    
};


function testDix(valeur) {
    if (valeur < 10) {
        return "0" + valeur;
    } else {
        return valeur;
    }
}