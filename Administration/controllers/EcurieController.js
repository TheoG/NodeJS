var model = require('../models/ecurie.js'),
    pays = require('../models/pays.js'),
    pneu = require('../models/pneu.js'),
    course = require('../models/course.js'),
    grandprix = require('../models/grandprix.js'),
    sponsor = require('../models/sponsor.js'),
    photo = require('../models/photo.js'),
    pilote = require('../models/pilote.js'),
    voiture = require('../models/voiture.js'),
    
    fs   = require('fs-extra'),
    path = require("path"),
    formidable = require('formidable'),
    util = require('util'),
    async = require('async');


module.exports.GestionEcurie = function(request, response) {
    response.title = "Gestion des écuries";
    
    
    if (request.session.updateOk) {
        response.updateSuccess = true;
        request.session.updateOk = null;
    } else if (request.session.ajoutOk) {
        response.insertSuccess = true;
        request.session.ajoutOk = null;
    } else if (request.session.supprOk) {
        response.supprSuccess = true;
        request.session.supprOk = null;
    }
    
    
    model.getListeEcurie(function(err, result) {
        if (err) {
            console.log(err);
            return;
        }
        
        response.ecurie = result;
        response.render("gestionEcurie", response);
    });
}; 

module.exports.AjouterEcurie = function(request, response) {
    response.title = "Ajouter une écurie";

    async.parallel([
        function(callback) {
            pneu.getListeFournisseur(function(err, result) {callback(null, result)});
        },
        function(callback) {
            pays.getListePays(function(err, result) {callback(null, result)});
        }
    ],
    function(err, result) {
        if (err) {
            console.log(err);
            return;
        }

        response.pneu = result[0];
        response.pays = result[1];
  
        response.render("ajouterEcurie", response);
    });
};

module.exports.VerifierAjout = function(request, response) {
    
    var form = new formidable.IncomingForm();
    form.parse(request, function(err, fields, files) {
    
        var valeurs = {
            ECUNOM:             fields.nom,
            ECUNOMDIR:          fields.directeur,
            ECUADRSIEGE:        fields.adresse,
            ECUPOINTS:          fields.points,
            PAYNUM:             fields.pays,
            ECUADRESSEIMAGE:    files.image.name,
            FPNUM:              fields.pneu
        };
    
        model.addEcurie(valeurs, function(err, result) {
            if (err) {
                console.log(err);
                return;
            }

            request.session.insertOk = true;
            
            response.redirect("../ecuries");
        });
    });
    
    form.on('progress', function(bytesReceived, bytesExpected) {
        var percent_complete = (bytesReceived / bytesExpected); 
        global.socket.emit('chargement', percent_complete);
    });
    
    form.on('end', function (fields, files) {
        var temp_path = this.openedFiles[0].path;
        var file_name = this.openedFiles[0].name;
        var new_location = path.join(__dirname, '../../Public/image/ecurie/');
        fs.copy(temp_path, new_location + file_name, function (err) {
            if (err) {
                console.error(err);
            } else {
                fs.unlink(temp_path, function(err) {
                    if (err) {
                        console.log(err);
                    }
                }); 
            }
        });
    });
};


module.exports.ModifierEcurie = function(request, response) {
    response.title = "Modifier une écurie";
    var numero = request.params.numero;
    
    async.parallel([
        function(callback) {
            pays.getListePays(function(err, result) {callback(null, result)});
        },
        
        function(callback) {
            pneu.getListeFournisseur(function(err, result) {callback(null, result)});
        },
        
        function(callback) {
            model.getDetailEcurie(numero, function(err, result) {callback(null, result)});
        }
    ],
                  
    function(err, result) {
        if (err) {
            console.log(err);
            return;
        }
        
        response.pays = result[0];
        response.pneu = result[1];
        response.ecurie = result[2][0];
        
        response.render("ajouterEcurie", response);
    });
};

module.exports.VerifierModification = function(request, response) {
    var numero = request.params.numero;
    
    
    var form = new formidable.IncomingForm();
    form.parse(request, function(err, fields, files) {
        var valeurs = {
            ECUNOM:             fields.nom,
            ECUNOMDIR:          fields.directeur,
            ECUADRSIEGE:        fields.adresse,
            ECUPOINTS:          fields.points,
            PAYNUM:             fields.pays,
            FPNUM:              fields.pneu     
        };

        model.updateEcurie(valeurs, numero, function(err, result) {
            if (err) {
                console.log(err);
                return;
            }

            request.session.updateOk = true;
            response.redirect("../../ecuries");
        });
    });
};


/*
* Fonction supprimant l'ensemble des données attachées à une écurie donnée
*/
module.exports.SupprimerEcurie = function(request, response) {
    var num = request.params.numero;
    
    async.series([
        function(callback) {
            model.getPhoto(num, function(err, result) {callback(null, result)});
        },
        function(callback) {
            course.supprimerEcurie(num, function(err, result) {callback(null, result)});
        },
        function(callback) {
            photo.supprimerPiloteEcurie(num, function(err, result) {callback(null, result)});
        },
        function(callback) {
            sponsor.supprimerSponsorEcurie(num, function(err, result) {callback(null, result)});
        },
        function(callback) {
            sponsor.supprimerFinanceEcurie(num, function(err, result) {callback(null, result)});
        },
        function(callback) {
            voiture.supprimerEcurie(num, function(err, result) {callback(null, result)});
        },
        function(callback) {
            pilote.supprimerEcurie(num, function(err, result) {callback(null, result)});
        },
        function(callback) {
            model.supprimerEcurie(num, function(err, result) {callback(null, result)});
        }
    ],

    function(err, result) {
        if (err) {
            console.log(err);
            return;
        }
        
        // On supprime également les images du serveur
        var new_location = path.join(__dirname, '../../Public/image/ecurie/');
        fs.remove(new_location + result[0][0].ECUADRESSEIMAGE);
        
        request.session.supprOk = true;
        response.redirect("../../ecuries");
    });
};
