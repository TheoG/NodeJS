var model = require("../models/circuit.js"),
    course = require("../models/course.js"),
    pays = require("../models/pays.js"),
    grandprix = require("../models/grandprix.js"),
    
    formidable = require('formidable'),
    util = require('util'),
    fs   = require('fs-extra'),
    path = require("path"),
    async = require("async");


/*
*   Fichier de gestion des circuits
*
*/

// Récupère l'ensemble des données pour l'écran principal des circuits
module.exports.GestionCircuit = function(request, response) {
    response.title = "Gestion des circuits";

    // Gestion des toastr en cas d'ajout, suppression etc.
    if (request.session.updateOk) {
        response.updateSuccess = true;
        request.session.updateOk = null;
    } else if (request.session.ajoutOk) {
        response.insertSuccess = true;
        request.session.ajoutOk = null;
    } else if (request.session.supprOk) {
        response.supprSuccess = true;
        request.session.supprOk = null;
    }
    
    model.getListeCircuit(function(err, result) {
        if (err) {
            console.log(err);
            return;
        }
        
        // Gestion d'un affichage propre pour les grands nombres
        for (var r in result) {
            result[r].CIRLONGUEUR = formaterNombre(result[r].CIRLONGUEUR);
            result[r].CIRNBSPECTATEURS = formaterNombre(result[r].CIRNBSPECTATEURS);
        }
        
        
        response.circuits = result;
        response.render("gestionCircuit", response);
    });
}; 

// Formulaire d'ajout d'un circuit
module.exports.AjouterCircuit = function(request, response) {
    response.title = "Ajouter un circuit";

    pays.getListePays(function(err, result) {
        if (err) {
            console.log(err);
            return;
        } 

        response.pays = result;
        response.render("ajouterCircuit", response);
    });
};

// Validation des données, et appel de l'insertion  
module.exports.VerifierAjout = function(request, response) {

    // Pour l'upload de fichier
    var form = new formidable.IncomingForm();
    form.parse(request, function(err, fields, files) {

        // Les valeurs à insérer
        var valeurs = {
            CIRNOM:             fields.nom,
            CIRLONGUEUR:        fields.longueur,
            PAYNUM:             fields.pays,
            CIRADRESSEIMAGE:    files.image.name,
            CIRNBSPECTATEURS:   fields.nbSpectateur,
            CIRTEXT:            fields.desc        
        };  

        model.addCircuit(valeurs, function(err, result) {
            if (err) {
                console.log(err);
                return;
            }

            request.session.ajoutOk = true;
            
            response.redirect("../circuits");
        });

    });

    // Envoi du pourcentage de l'upload de l'image
    form.on('progress', function(bytesReceived, bytesExpected) {
        var percent_complete = (bytesReceived / bytesExpected); 
        global.socket.emit('chargement', percent_complete);
    });

    // Fin de l'envoi, on effectue la copie des fichiers de la zone temporaire au dossier public du serveur
    form.on('end', function (fields, files) {
        
        // Emplacement du fichier temporaire
        var temp_path = this.openedFiles[0].path;
        
        // Nom du fichier
        var file_name = this.openedFiles[0].name;
        
        // On set l'emplacement dans le dossier public
        var new_location = path.join(__dirname, '../../Public/image/circuit/');
        
        // On effectue la copie
        fs.copy(temp_path, new_location + file_name, function (err) {
            if (err) {
                console.error(err);
            } else {
                // On supprime les fichiers temporaires qui ont servis à l'upload
                fs.unlink(temp_path, function(err) {
                    if (err) {
                        console.log(err);
                    }
                }); 
            }
        });
    });
};

// Formulaire pour la modification d'un circuit
module.exports.ModifierCircuit = function(request, response) {
    response.title = "Modifier un circuit";

    var numero = request.params.numero;

    async.parallel([
        function(callback) {
            pays.getListePays(function(err, result) {callback(null, result)});
        },

        function(callback) {
            model.getDetailCircuit(numero, function(err, result) {callback(null, result)});
        }
    ],

    function(err, result) {
        if (err) {
            console.log(err);
            return;
        }

        response.pays = result[0];
        response.circuit = result[1][0];
        response.render("ajouterCircuit", response);
    });
};


// Validation des valeurs
module.exports.VerifierModification = function(request, response) {
    var numero = request.params.numero;

    var form = new formidable.IncomingForm();
    form.parse(request, function(err, fields, files) {
        var valeurs = {
            CIRNOM:             fields.nom,
            CIRLONGUEUR:        fields.longueur,
            PAYNUM:             fields.pays,
            CIRNBSPECTATEURS:   fields.nbSpectateur,
            CIRTEXT:            fields.desc      
        };
    
        model.updateCircuit(valeurs, numero, function(err, result) {
            if (err) {
                console.log(err);
                return;
            }

            request.session.updateOk = true;
            response.redirect("../../circuits");
        });
    });
};

// Suppression d'un circuit
module.exports.SupprimerCircuit = function(request, response) {
    var num = request.params.numero;

    async.series([
        function(callback) {
            model.getImageCircuit(num, function(err, result) {callback(null, result)});
        },
        function(callback) {
            course.supprimerGrandPrix(num, function(err, result) {callback(null, result)});
        },
        function(callback) {
            grandprix.supprimerGrandPrix(num, function(err, result) {callback(null, result)});
        },
        function(callback) {
            model.supprimerCircuit(num, function(err, result) {callback(null, result)});
        }
    ],

    function(err, result) {
        if (err) {
            console.log(err);
            return;
        }
        
        // On supprime les images du serveur
        var new_location = path.join(__dirname, '../../Public/image/circuit/');
        fs.remove(new_location + result[0][0].CIRADRESSEIMAGE);

        request.session.supprOk = true;
        response.redirect("../../circuits");
    });
};

/*
* Fonction formattant un nombre en ajoutant des espaces entre les milliers / millions, etc.
* Entrée : Un nombre
* Sortie : Une string au bon format
*/
function formaterNombre(nbr) {
    nombre = ''+nbr;
    var count = 0;
    var retour = '';
    
    for (var i = nombre.length - 1; i >= 0; i--) {
        if(count !=0  && count % 3 == 0)
            retour = nombre[i]+' '+retour ;
         else
            retour = nombre[i]+retour ;
         count++;
    }
    return retour;
}