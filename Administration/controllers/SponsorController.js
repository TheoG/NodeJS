var model = require("../models/sponsor.js"),
    ecurie = require("../models/ecurie.js"),
    async = require("async");

module.exports.GestionSponsor = function(request, response) {
    response.title = "Gestion des sponsors";

    model.getListeSponsors(function(err, result) {
        if (err) {
            console.log(err);
            return;
        }

        // Gestion des toastr en cas d'ajout, suppression etc.
        if (request.session.updateOk) {
            response.updateSuccess = true;
            request.session.updateOk = null;
        } else if (request.session.ajoutOk) {
            response.insertSuccess = true;
            request.session.ajoutOk = null;
        } else if (request.session.supprOk) {
            response.supprSuccess = true;
            request.session.supprOk = null;
        }
    
        
        response.sponsors = result;
        response.render("gestionSponsor", response);
    });
};

module.exports.AjouterSponsor = function(request, response) {
    response.title = "Ajouter un sponsor";

    ecurie.getListeEcurie(function(err, result) {
        if (err) {
            console.log(err);
            return;
        } 

        response.ecurie = result;
        response.render("ajouterSponsor", response);
    });
};

module.exports.VerifierAjout = function(request, response) {
    var valeurs = {
        SPONOM:             request.body.nom,
        SPOSECTACTIVITE:    request.body.secteur
    };

    var finance = {
        ECUNUM: request.body.ecurie,
        SPONUM: '0'
    }

    model.addSponsor(valeurs, function(err, result) {
        if (err) {
            console.log(err);
            return;
        }

        if (finance.ECUNUM != '0') {

            finance.SPONUM = result.insertId;

            model.sponsoriseEcurie(finance, function(err, result) {
                if (err) {
                    console.log(err);
                    return;
                } 
            });
        }
    });
    request.session.ajoutOk = true;
    response.redirect("../sponsors");
};

module.exports.ModifierSponsor = function(request, response) {
    response.title = "Modifier un sponsor";

    var numero = request.params.numero;

    async.parallel([
        function(callback) {
            ecurie.getListeEcurie(function(err, result) {callback(null, result)});
        },

        function(callback) {
            model.getDetailSponsor(numero, function(err, result) {callback(null, result)});
        }
    ],

    function(err, result) {
        if (err) {
            console.log(err);
            return;
        }

        response.ecurie = result[0];
        response.sponsor = result[1][0];

        response.render("ajouterSponsor", response);
    });
};


module.exports.VerifierModification = function(request, response) {

    var numero = request.params.numero;

    var valeurs = {
        SPONOM:             request.body.nom,
        SPOSECTACTIVITE:    request.body.secteur
    };

    var finance = {
        ECUNUM: request.body.ecurie,
        SPONUM: numero
    }
    
    async.series([
        function(callback) {
            model.supprimerSponsoriseEcurie(numero, function(err, result) {callback(null, result)});
        },
        
        function(callback) {
            if (finance.ECUNUM != '0') {
                model.sponsoriseEcurie(finance, function(err, result) {callback(null, result)});
            } else {
                callback(null, null);
            }
        }
    ],
    
    function(err, result) {
        if (err) {
            console.log(err);
            return;
        }
        request.session.updateOk = true;
        response.redirect("../../sponsors");
    });

};


module.exports.SupprimerSponsor = function(request, response) {
    var num = request.params.numero;

    async.series([
        function(callback) {
            model.supprimerFinance(num, function(err, result) {callback(null, result)});
        },

        function(callback) {
            model.supprimerSponsorise(num, function(err, result) {callback(null, result)});
        },

        function(callback) {
            model.supprimerSponsor(num, function(err, result) {callback(null, result)});
        }
    ],

                 function(err, result) {
        if (err) {
            console.log(err);
            return;
        }

        request.session.supprOk = true;
        response.redirect("../../sponsors");
    });
};