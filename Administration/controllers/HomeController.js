var model = require("../models/grandprix.js");

// ////////////////////////////////////////////// A C C U E I L
module.exports.Index = function(request, response) {
    response.title = "Bienvenue sur le site de WROOM (IUT du Limousin).";
    
    response.render('home', response);
};

module.exports.PageNonTrouvee = function(request, response) {
    response.title = "Page non trouvée";
    
    response.render('404', response);
};