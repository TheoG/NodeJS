var model = require("../models/pilote.js"),
    pays = require("../models/pays.js"),
    photo = require("../models/photo.js"),
    course = require("../models/course.js"),
    sponsor = require("../models/sponsor.js"),
    grandprix = require("../models/grandprix.js"),
    ecurie = require("../models/ecurie.js");

var async = require("async"),
    fs   = require('fs-extra'),
    moment = require('moment'),
    formidable = require('formidable'),
    path = require("path");

// ///////////////////////// R E P E R T O I R E    D E S    P I L O T E S

module.exports.GestionPilote = function(request, response, next) {
    response.title = "Gestion des pilotes";

    if (request.session.updateOk) {
        response.updateSuccess = true;
        request.session.updateOk = null;
    } else if (request.session.ajoutOk) {
        response.insertSuccess = true;
        request.session.ajoutOk = null;
    } else if (request.session.supprOk) {
        response.supprSuccess = true;
        request.session.supprOk = null;
    }
    
    model.getListePilote(function(err, result) {
        if (err) {
            console.log(err);
            return next(err);
        }
        
        
        response.pilotes = result;
        response.render("gestionPilote", response);
    });
};

module.exports.AjouterPilote = function(request, response) {
    response.title = "Ajouter un pilote";
    
    async.parallel([
        function(callback) {
            pays.getListeNationalite(function(err, result) {callback(null, result)});
        },
        
        function(callback) {
            ecurie.getListeEcurie(function(err, result) {callback(null, result)});
        }
    ],
                  
    function(err, result) {
        if (err) {
            console.log(err);
            return;
        }
        
        response.nationalite = result[0];
        response.ecurie = result[1];
        response.render("ajouterPilote", response);
    });
};

module.exports.VerifierAjout = function(request, response) {
    
    var form = new formidable.IncomingForm();
    form.parse(request, function(err, fields, files) {
    
        var valeurs = {
            PILPRENOM:      fields.prenom,
            PILNOM:         fields.nom,
            PILDATENAIS:    fields.dateNaissAnnee + "-" + fields.dateNaissMois + "-" + fields.dateNaissJour,
            PAYNUM:         fields.nationalite,
            ECUNUM:         fields.ecurie,
            PILPOINTS:      fields.points,
            PILPOIDS:       fields.poids,
            PILTAILLE:      fields.taille,
            PILTEXTE:       fields.desc        
        };
        

        model.createPilote(valeurs, function(err, result) {
            if (err) {
                console.log(err);
                return;
            }
        
            var photo = {
                PHONUM:         1,
                PILNUM:         result.insertId,    // On récupère l'ID qui vient d'être inséré en base
                PHOSUJET:       fields.sujetImage,
                PHOCOMMENTAIRE: fields.commentaireImage,
                PHOADRESSE:     files.image.name
            };
            
            model.ajouterPhoto(photo, function(err, result) {
                if (err) {
                    console.log(err);
                    return;
                }
            }); 
        });
        
        request.session.ajoutOk = true;
        response.redirect("../pilote");
    });
    
    form.on('progress', function(bytesReceived, bytesExpected) {
        var percent_complete = (bytesReceived / bytesExpected); 
        global.socket.emit('chargement', percent_complete);
    });
    
    form.on('end', function (fields, files) {
        var temp_path = this.openedFiles[0].path;
        var file_name = this.openedFiles[0].name;
        var new_location = path.join(__dirname, '../../Public/image/pilote/');
        fs.copy(temp_path, new_location + file_name, function (err) {
            if (err) {
                console.error(err);
            } else {
                fs.unlink(temp_path, function(err) {
                    if (err) {
                        console.log(err);
                    }
                }); 
            }
        });
    });
};


module.exports.ModifierPilote = function(request, response) {
    response.title = "Modifier un pilote";
    var numero = request.params.numero;
    
    async.parallel([
        function(callback) {
            pays.getListeNationalite(function(err, result) {callback(null, result)});
        },
        
        function(callback) {
            ecurie.getListeEcurie(function(err, result) {callback(null, result)});
        },
        
        function(callback) {
            model.getDetailPilote(numero, function(err, result) {callback(null, result)});
        }
    ],
                  
    function(err, result) {
        if (err) {
            console.log(err);
            return;
        }

        var dateNaiss = moment(result[2][0].PILDATENAIS).format("DD/MM/YYYY");

        var dateNaiss = {
            jour: moment(result[2][0].PILDATENAIS).format("DD"),
            mois: moment(result[2][0].PILDATENAIS).format("MM"),
            annee: moment(result[2][0].PILDATENAIS).format("YYYY")
        }
        
        response.nationalite = result[0];
        response.ecurie = result[1];
        response.pilote = result[2][0];
        response.dateNaiss = dateNaiss;
        response.render("ajouterPilote", response);
    });
};

module.exports.VerifierModification = function(request, response) {
    var numero = request.params.numero;
    
    
    var form = new formidable.IncomingForm();
    form.parse(request, function(err, fields, files) {
    
        var valeurs = {
            PILPRENOM:      fields.prenom,
            PILNOM:         fields.nom,
            PILDATENAIS:    fields.dateNaissAnnee + "-" + fields.dateNaissMois + "-" + fields.dateNaissJour + "-",
            PAYNUM:      fields.nationalite,
            ECUNUM:         fields.ecurie,
            PILPOINTS:      fields.points,
            PILPOIDS:       fields.poids,
            PILTAILLE:      fields.taille,
            PILTEXTE:        fields.desc        
        };

        model.updatePilote(valeurs, numero, function(err, result) {
            if (err) {
                console.log(err);
                return;
            }

            request.session.updateOk = true;
            response.redirect("../../pilote");
        });
    });
};

module.exports.SupprimerPilote = function(request, response) {
    var num = request.params.numero;
    
    async.series([
        function(callback) {
            photo.getToutesPhotosPilote(num, function(err, result) {callback(null, result)});
        },
        function(callback) {
            course.supprimerPilote(num, function(err, result) {callback(null, result)});
        },
        function(callback) {
            photo.supprimerPilote(num, function(err, result) {callback(null, result)});
        },
        function(callback) {
            sponsor.supprimerPilote(num, function(err, result) {callback(null, result)});
        },
        
        function(callback) {
            model.supprimerPilote(num, function(err, result) {callback(null, result)});
        }
    ],

    function(err, result) {
        if (err) {
            console.log(err);
            return;
        }
        
        var new_location = path.join(__dirname, '../../Public/image/pilote/');
        result[0].forEach(function(item) {
            fs.remove(new_location + item.PHOADRESSE);
        });
        
        request.session.supprOk = true;
        response.redirect("../../pilote");
    });
};


module.exports.AjouterPhoto = function(request, response) {
    response.title = "Ajouter une photo";
    
    model.getDetailPilote(request.params.numero, function(err, result) {
        if (err) {
            console.log(err);
            return next(err);
        }
                
        response.pilote = result[0];
        response.render("ajouterPhotoPilote", response);
    });
    
};

module.exports.VerifierAjoutPhoto = function(request, response) {
    
    var numero = request.params.numero;
    
    
    var form = new formidable.IncomingForm();
    form.parse(request, function(err, fields, files) {
    
        var valeurs = {
            PHONUM: 0,
            PILNUM: numero,
            PHOSUJET: fields.sujet,
            PHOCOMMENTAIRE: fields.commentaire,
            PHOADRESSE: files.image.name
        };

        photo.nbPhotoPilote(numero, function(err, result) {
            if (err) {
                console.log(err);
                return;
            }
                        
            valeurs.PHONUM = result[0].NB + 1;
            
            photo.ajouterPhoto(valeurs, function(err, result) {
                if (err) {
                    console.log(err);
                    return;
                }
            });
        });
        
        
        response.redirect("../../pilote");
    });
    
    form.on('progress', function(bytesReceived, bytesExpected) {
        var percent_complete = (bytesReceived / bytesExpected); 
        global.socket.emit('chargement', percent_complete);
    });
    
    form.on('end', function (fields, files) {
        var temp_path = this.openedFiles[0].path;
        var file_name = this.openedFiles[0].name;
        var new_location = path.join(__dirname, '../../Public/image/pilote/');
        fs.copy(temp_path, new_location + file_name, function (err) {
            if (err) {
                console.error(err);
            }
            
            fs.remove(temp_path);
        });
    });
};