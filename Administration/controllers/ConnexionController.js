var model = require("../models/connexion.js"),
    crypto = require("crypto");


module.exports.SeConnecter = function(request, response) {
    response.title = "Connexion";

    // Gestion de l'affichage sur la page de connexion
    response.estConnecte = request.session.estConnecte;
    response.estVerifie = false;
    response.tentative = false;
    
    response.render("seConnecter", response);
};


module.exports.SeDeconnecter = function(request, response) {
    request.session.destroy();
    
    response.render("seConnecter", response);
};

// Vérification de l'authentification
module.exports.VerifierConnexion = function(request, response) {
    response.title = "Authentification";

    var login = request.body.login;
    var pwd = request.body.pwd;

    // On encrypte le mot de passe pour le tester
    pwd = crypto.createHash("sha1").update(pwd).digest('hex')
    
    model.isValid(function(err, result) {
        if (err) {
            console.log(err);
            return;
        }

        response.circuit = result;
        
        response.tentative = true;

        
        if (login == result[0]["LOGIN"] && pwd == result[0]["PASSWD"]) {
            response.estVerifie = true;
            response.estConnecte = true;
            request.session.estConnecte = true;
        } else {
            response.estVerifie = false;
            response.estConnecte = false;
            request.session.estConnecte = false;
        }
        
        response.render("seConnecter", response);
    });
};