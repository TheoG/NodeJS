var db = require('../configDb');

module.exports.ajouterPhoto = function(valeurs, callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "INSERT INTO photo SET ?";

            connexion.query(sql, valeurs, callback);

            connexion.release();
        } 
    });
};


module.exports.getPhotoID = function(numero, callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "SELECT PHONUM, PHOSUJET, PHOCOMMENTAIRE, PHOADRESSE FROM photo WHERE PHONUM = 1 AND PILNUM = " + numero;
            
            connexion.query(sql, callback);
            
            connexion.release();
        } 
    });
};

module.exports.getPhotoPilote = function(numero, callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "SELECT PHONUM, PHOSUJET, PHOCOMMENTAIRE, PHOADRESSE FROM photo WHERE PHONUM != 1 AND PILNUM = " + numero;
            
            connexion.query(sql, callback);
            
            connexion.release();
        } 
    });  
};

module.exports.getToutesPhotosPilote = function(numero, callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "SELECT PHONUM, PHOADRESSE FROM photo WHERE PILNUM = " + numero;
            
            connexion.query(sql, callback);
            
            connexion.release();
        } 
    });  
};

module.exports.getToutesPhotosPilote = function(numero, callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "SELECT PHONUM, PHOADRESSE FROM photo WHERE PILNUM = " + numero;
            
            connexion.query(sql, callback);
            
            connexion.release();
        } 
    });  
};

module.exports.nbPhotoPilote = function(numero, callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "SELECT COUNT(*) as NB FROM photo WHERE PILNUM = " + numero;

            connexion.query(sql, callback);

            connexion.release();
        }
    });
};

module.exports.supprimerPilote = function(numero, callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "DELETE FROM photo WHERE PILNUM = " + numero;

            connexion.query(sql, callback);

            connexion.release();
        }
    });
};

module.exports.supprimerPiloteEcurie = function(numero, callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "DELETE FROM photo WHERE PILNUM IN (SELECT PILNUM FROM pilote WHERE ECUNUM = " + numero + ")";

            connexion.query(sql, callback);

            connexion.release();
        }
    });
};