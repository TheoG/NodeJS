var db = require('../configDb');


module.exports.ajouterPhoto = function(valeurs, callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "INSERT INTO photo SET ?";

            connexion.query(sql, valeurs, callback);

            connexion.release();
        } 
    });
};


module.exports.getListePilote = function(callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "SELECT PILNUM, PILNOM, PILPRENOM, PILDATENAIS FROM pilote ORDER BY PILNOM";

            connexion.query(sql, callback);

            connexion.release();
        }
    });
};

module.exports.getListePiloteAvecEcurie = function(numero, callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "SELECT PILNUM, PILNOM, PILPRENOM, PILDATENAIS FROM pilote WHERE ECUNUM IS NOT NULL AND PILNUM NOT IN (SELECT PILNUM FROM course WHERE GPNUM = " + numero + " ) ORDER BY PILNOM";

            connexion.query(sql, callback);
            
            connexion.release();
        }
    });
};

module.exports.createPilote = function(valeurs, callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "INSERT INTO pilote SET ?";
            
            connexion.query(sql, valeurs, callback);
            
            connexion.release();
        }
    });
};

module.exports.supprimerPilote = function(numero, callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "DELETE FROM pilote WHERE PILNUM = " + numero;
            
            connexion.query(sql, callback);
            
            connexion.release();
        } 
    });
};

module.exports.supprimerPoints = function(numero, callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "UPDATE pilote SET PILPOINTS = NULL" + numero;
            
            connexion.query(sql, callback);
            
            connexion.release();
        } 
    });
};


module.exports.updatePilote = function(valeurs, numero, callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "UPDATE pilote SET ? WHERE PILNUM = " + numero;
            
            connexion.query(sql, valeurs, callback);
            
            connexion.release();
        }      
    });
};

module.exports.updatePoints = function(pilote, points, callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "UPDATE pilote SET PILPOINTS = " + points + " WHERE PILNUM = " + pilote;
            
            connexion.query(sql, callback);
            
            connexion.release();
        }      
    });
};


module.exports.getDetailPilote = function(numero, callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "SELECT PILNUM, PILNOM, PILPRENOM, PILDATENAIS, PILPOINTS, PILPOIDS, PILTAILLE, PILTEXTE, ECUNUM, PAYNUM FROM pilote WHERE PILNUM = " + numero;
            
            connexion.query(sql, callback);
            
            connexion.release();
        }      
    });
};

module.exports.supprimerEcurie = function(numero, callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "DELETE FROM pilote WHERE ECUNUM = " + numero;
                        
            connexion.query(sql, callback);
            
            connexion.release();
        }      
    });
};
