var db = require('../configDb');

module.exports.getListeFournisseur = function(callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "SELECT FPNUM, FPNOM FROM fourn_pneu ORDER BY FPNOM";
            
            connexion.query(sql, callback);
            
            connexion.release();
        }
    });
};