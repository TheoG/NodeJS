var db = require('../configDb');

module.exports.getListeSponsors = function(callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "SELECT SPONUM, SPONOM, SPOSECTACTIVITE FROM sponsor ORDER BY SPONOM";
            
            connexion.query(sql, callback);
            
            connexion.release();
        }      
    });
};

module.exports.getSponsorNumPilote = function(number, callback) {

    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "SELECT SPONOM, SPOSECTACTIVITE FROM sponsor s JOIN sponsorise ss ON s.sponum = ss.sponum WHERE ss.pilnum = " + number;
            
            connexion.query(sql, callback);
            
            connexion.release();
        }
    });
};

module.exports.supprimerPilote = function(numero, callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "DELETE FROM sponsorise WHERE PILNUM = " + numero;

            connexion.query(sql, callback);

            connexion.release();
        }
    });
};


module.exports.addSponsor = function(valeurs, callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "INSERT INTO sponsor SET ?";
            
            connexion.query(sql, valeurs, callback);
            
            connexion.release();
        }
    });
};


module.exports.sponsoriseEcurie = function(valeurs, callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "INSERT INTO finance SET ?";
            
            connexion.query(sql, valeurs, callback);
            
            connexion.release();
        } 
    });
};


module.exports.getDetailSponsor = function(numero, callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "SELECT s.SPONUM, s.SPONOM, s.SPOSECTACTIVITE, f.ECUNUM FROM sponsor s LEFT JOIN finance f  ON s.SPONUM = f.SPONUM WHERE s.SPONUM = " + numero;

            connexion.query(sql, callback);
            
            connexion.release();
        } 
    });
};

//TODO a supprimer ?
module.exports.updateSponsor = function(numero, valeurs, callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "UPDATE sponsor SET ? WHERE SPONUM = " + numero;
            
            connexion.query(sql, valeurs, callback);
            
            connexion.release();
        }
    });
};

module.exports.supprimerSponsoriseEcurie = function(numero, callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "DELETE FROM finance WHERE SPONUM = " + numero;
            
            connexion.query(sql, callback);
            
            connexion.release();
        } 
    });
};


module.exports.supprimerSponsor = function(numero, callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "DELETE FROM sponsor WHERE SPONUM = " + numero;
            
            connexion.query(sql, callback);
            
            connexion.release();
        } 
    });
};

module.exports.supprimerFinance = function(numero, callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "DELETE FROM finance WHERE SPONUM = " + numero;
            
            connexion.query(sql, callback);
            
            connexion.release();
        } 
    });
};

module.exports.supprimerSponsorise = function(numero, callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "DELETE FROM sponsorise WHERE SPONUM = " + numero;
            
            connexion.query(sql, callback);
            
            connexion.release();
        } 
    });
};


module.exports.supprimerSponsorEcurie = function(numero, callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "DELETE FROM sponsorise WHERE PILNUM IN (SELECT PILNUM FROM pilote WHERE ECUNUM = " + numero + ")";

            connexion.query(sql, callback);

            connexion.release();
        }
    });
};


module.exports.supprimerFinanceEcurie = function(numero, callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "DELETE FROM finance WHERE ECUNUM = " + numero;

            connexion.query(sql, callback);

            connexion.release();
        }
    });
};