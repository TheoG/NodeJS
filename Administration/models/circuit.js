var db = require("../configDb");


module.exports.getListeCircuit = function(callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "SELECT CIRNOM, CIRNUM, CIRLONGUEUR, CIRNBSPECTATEURS FROM circuit ORDER BY CIRNOM";

            connexion.query(sql, callback);

            connexion.release();
        } 
    });
};

module.exports.getDetailCircuit = function(numero, callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "SELECT CIRNOM, PAYNUM, CIRNOM, CIRLONGUEUR, CIRNBSPECTATEURS, CIRADRESSEIMAGE, CIRTEXT FROM circuit WHERE CIRNUM = " + numero;

            connexion.query(sql, callback);

            connexion.release();
        } 
    });
};



module.exports.addCircuit = function(valeurs, callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "INSERT INTO circuit SET ?";

            connexion.query(sql, valeurs, callback);

            connexion.release();
        } 
    });
};

module.exports.updateCircuit = function(valeurs, numero, callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "UPDATE circuit SET ? WHERE CIRNUM = " + numero;

            connexion.query(sql, valeurs, callback);

            connexion.release();
        }      
    });
};

module.exports.supprimerCircuit = function(numero, callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "DELETE FROM circuit WHERE CIRNUM = " + numero;

            connexion.query(sql, callback);

            connexion.release();
        }      
    });
};

module.exports.getImageCircuit = function(numero, callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "SELECT CIRADRESSEIMAGE FROM circuit WHERE CIRNUM = " + numero;
            
            connexion.query(sql, callback);

            connexion.release();
        }
    });
};