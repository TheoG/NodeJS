/*
* config.Db contient les parametres de connection à la base de données
* il va créer aussi un pool de connexions utilisables
* sa méthode getConnection permet de se connecter à MySQL
*
*/
var db = require('../configDb');

/*
* Récupérer l'intégralité les écuries avec l'adresse de la photo du pays de l'écurie
* @return Un tableau qui contient le N°, le nom de l'écurie et le nom de la photo du drapeau du pays
*/
module.exports.getListeEcurie = function (callback) {
    // connection à la base
    db.getConnection(function(err, connexion){
        if(!err){
            var sql = "SELECT ECUNUM, ECUNOM, ECUNOMDIR, ECUPOINTS FROM ecurie ORDER BY ECUNOM";

            connexion.query(sql, callback);

            connexion.release();
        }
    });
};

module.exports.addEcurie = function(valeurs, callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "INSERT INTO ecurie SET ?";
            
            connexion.query(sql, valeurs, callback);
            
            connexion.release();
        }
    });
};


module.exports.getDetailEcurie = function(numero, callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "SELECT ECUNUM, FPNUM, ECUNOM, ECUNOMDIR, ECUADRSIEGE, ECUPOINTS, PAYNUM FROM ecurie WHERE ECUNUM = " + numero;
            
            connexion.query(sql, callback);
            
            connexion.release();
        } 
    });
};

module.exports.updateEcurie = function(valeurs, numero, callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "UPDATE ecurie SET ? WHERE ECUNUM = " + numero;
            
            connexion.query(sql, valeurs, callback);
            
            connexion.release();
        }      
    });
};


module.exports.getPhoto = function(numero, callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "SELECT ECUADRESSEIMAGE FROM ecurie WHERE ECUNUM = " + numero;
            
            connexion.query(sql, callback);
            
            connexion.release();
        }      
    });
};

module.exports.updatePoints = function(ecurie, points, callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "UPDATE ecurie SET ECUPOINTS = ECUPOINTS + " + points + " WHERE ECUNUM = " + ecurie;
            
            connexion.query(sql, callback);
            
            connexion.release();
        }      
    });
};


module.exports.supprimerEcurie = function(numero, callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "DELETE FROM ecurie WHERE ECUNUM = " + numero;
            
            connexion.query(sql, callback);

            connexion.release();
        }      
    });
};