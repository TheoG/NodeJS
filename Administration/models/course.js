var db = require('../configDb');

module.exports.supprimerPilote = function(numero, callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "DELETE FROM course WHERE PILNUM = " + numero;

            connexion.query(sql, callback);

            connexion.release();
        }
    });
};

module.exports.supprimerEcurie = function(numeroEcurie, callback) {
    db.getConnection(function(err, connexion){
        if (!err) {
            var sql = "DELETE FROM course WHERE PILNUM IN (SELECT PILNUM FROM pilote WHERE ECUNUM = " + numeroEcurie + ")";
            
            connexion.query(sql, callback);
            
            connexion.release();
        }
    });
};

module.exports.supprimerGrandPrix = function(numeroCircuit, callback) {
    db.getConnection(function(err, connexion){
        if (!err) {
            var sql = "DELETE FROM course WHERE GPNUM = (SELECT GPNUM FROM grandprix WHERE CIRNUM = " + numeroCircuit + ")";
            
            connexion.query(sql, callback);
            
            //TODO
        }
    });
};