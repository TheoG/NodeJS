var db = require("../configDb");

module.exports.getListeGrandPrix = function(callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "SELECT g.GPNUM, g.GPNOM, pa.PAYADRDRAP FROM grandprix g JOIN circuit c ON g.CIRNUM = c.CIRNUM JOIN pays pa ON pa.PAYNUM = c.PAYNUM ORDER BY g.GPNOM";
            
            connexion.query(sql, callback);
            
            connexion.release();
        } 
    });
};

module.exports.supprimerGrandPrix = function(numeroCircuit, callback) {
    db.getConnection(function(err, connexion){
        if (!err) {
            var sql = "DELETE FROM grandprix WHERE CIRNUM = " + numeroCircuit;
            
            connexion.query(sql, callback);
        }
    });
};

module.exports.supprimerResultat = function(pilnum, gpnum, callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "DELETE FROM course WHERE GPNUM = " + gpnum + " AND PILNUM = " + pilnum;     
        }
        
        connexion.query(sql, callback);
        
        connexion.release();
    });
};

module.exports.getClassementParGrandPrix = function(numero, callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "SELECT p.PILNUM, PILNOM, PILPRENOM, PILPOINTS, c.TEMPSCOURSE FROM pilote p JOIN course c ON c.PILNUM = p.PILNUM WHERE c.GPNUM = " + numero + " AND PILPOINTS IS NOT NULL ORDER BY c.TEMPSCOURSE";
            
            connexion.query(sql, callback);
            
            connexion.release();
        }      
    });
};

module.exports.getPoints = function(callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "SELECT PTPLACE, PTNBPOINTSPLACE FROM points ORDER BY PTPLACE";
            
            connexion.query(sql, callback);
            
            connexion.release();
        } 
    });
};

module.exports.ajouterResultat = function(valeurs, callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "INSERT INTO course SET ?";
            
            connexion.query(sql, valeurs, callback);
            
            connexion.release();
        }      
    });
};