var db = require("../configDb");

module.exports.getListeNationalite = function(callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "SELECT PAYNUM, PAYNAT FROM pays ORDER BY PAYNAT";
            
            connexion.query(sql, callback);
            
            connexion.release();
        } 
    });
};

module.exports.getListePays = function(callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "SELECT PAYNUM, PAYNOM FROM pays ORDER BY PAYNOM";
            
            connexion.query(sql, callback);
            
            connexion.release();
        } 
    });
};