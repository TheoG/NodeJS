var moment = require('moment');

function hbsHelpers(handlebars) {
    return handlebars.create({
        defaultLayout: 'main',

        partialsDir: ['views/partials/'],
        
        helpers: {
            formatDate: function (date, format) {
                return moment(date).format(format);
            },
            inc: function(value, options) {
                return parseInt(value) + 1;
            },
            ifCond : function (v1, operator, v2, options) {

                switch (operator) {
                    case '==':
                        return (v1 == v2) ? options.fn(this) : options.inverse(this);
                    case '===':
                        return (v1 === v2) ? options.fn(this) : options.inverse(this);
                    case '<':
                        return (v1 < v2) ? options.fn(this) : options.inverse(this);
                    case '<=':
                        return (v1 <= v2) ? options.fn(this) : options.inverse(this);
                    case '>':
                        return (v1 > v2) ? options.fn(this) : options.inverse(this);
                    case '>=':
                        return (v1 >= v2) ? options.fn(this) : options.inverse(this);
                    case '&&':
                        return (v1 && v2) ? options.fn(this) : options.inverse(this);
                    case '||':
                        return (v1 || v2) ? options.fn(this) : options.inverse(this);
                    default:
                        return options.inverse(this);
                }
            },

            for: function(from, to, incr, block) {
                var accum = '';
                for(var i = from; i < to; i += incr)
                    accum += block.fn(i);
                return accum;
            },
            
            rangPoint: function(points, index) {
                return parseInt(points[index].PTNBPOINTSPLACE);
            }
        }
    });
}

module.exports = hbsHelpers;