var HomeController = require('./../controllers/HomeController');
var ResultatController = require('./../controllers/ResultatController');
var EcurieController = require('./../controllers/EcurieController');
var PiloteController = require('./../controllers/PiloteController');
var CircuitController = require('./../controllers/CircuitController');
var ConnexionController = require('./../controllers/ConnexionController');
var SponsorController = require('./../controllers/SponsorController');

var verifAdminConnecte = function() {
    //    console.log(request.session.estConnecte);
    //    next();
}

// Routes
module.exports = function(app) {


    // Main Routes
    app.get('/', HomeController.Index);

    // connexion
    app.get('/login', ConnexionController.SeConnecter);
    app.post('/login', ConnexionController.VerifierConnexion);
    app.get('/seDeconnecter', ConnexionController.SeDeconnecter);

    // Vérification de la connexion de la personne avant de lui laisser la possibilité d'aller sur les différentes pages d'administration
    app.use(function(req, res, next) {
        if (req.session.estConnecte == false || req.session.estConnecte == undefined) {
            return res.redirect("/login");
        }
        next(); 
    });

    // pilotes
    app.get('/pilote', PiloteController.GestionPilote);
    app.get('/pilote/ajouterPilote', PiloteController.AjouterPilote);
    app.get('/pilote/supprimerPilote/:numero', PiloteController.SupprimerPilote);
    app.get('/pilote/modifierPilote/:numero', PiloteController.ModifierPilote);
    app.get('/pilote/ajouterPhoto/:numero', PiloteController.AjouterPhoto);
    app.post('/pilote/modifierPilote/:numero', PiloteController.VerifierModification);
    app.post('/pilote/ajouterPilote', PiloteController.VerifierAjout);
    app.post('/pilote/ajouterPhoto/:numero', PiloteController.VerifierAjoutPhoto);



    // circuits
    app.get('/circuits', CircuitController.GestionCircuit);
    app.get('/circuits/ajouterCircuit', CircuitController.AjouterCircuit);
    app.get('/circuits/supprimerCircuit/:numero', CircuitController.SupprimerCircuit);
    app.get('/circuits/modifierCircuit/:numero', CircuitController.ModifierCircuit);
    app.post('/circuits/modifierCircuit/:numero', CircuitController.VerifierModification);
    app.post('/circuits/ajouterCircuit', CircuitController.VerifierAjout);

    // Ecuries
    app.get('/ecuries', EcurieController.GestionEcurie);
    app.get('/ecuries/ajouterEcurie', EcurieController.AjouterEcurie);
    app.get('/ecuries/supprimerEcurie/:numero', EcurieController.SupprimerEcurie);
    app.get('/ecuries/modifierEcurie/:numero', EcurieController.ModifierEcurie);
    app.post('/ecuries/modifierEcurie/:numero', EcurieController.VerifierModification);
    app.post('/ecuries/ajouterEcurie', EcurieController.VerifierAjout);

    //Résultats
    app.get('/resultats', ResultatController.ListerResultat);
    app.post('/resultats/saisie', ResultatController.SaisieResultat);
    app.get('/resultats/saisie', ResultatController.SaisieResultat);
    app.post('/resultats/saisie/ajouterResultat', ResultatController.AjouterResultat);
    app.get('/resultats/saisie/supprimerResultat/:numero', ResultatController.SupprimerResultat);

    // Sponsors
    app.get('/sponsors', SponsorController.GestionSponsor);
    app.get('/sponsors/ajouterSponsor', SponsorController.AjouterSponsor);
    app.get('/sponsors/supprimerSponsor/:numero', SponsorController.SupprimerSponsor);
    app.get('/sponsors/modifierSponsor/:numero', SponsorController.ModifierSponsor);
    app.post('/sponsors/modifierSponsor/:numero', SponsorController.VerifierModification);
    app.post('/sponsors/ajouterSponsor', SponsorController.VerifierAjout);

    // tout le reste
    app.get('*', HomeController.PageNonTrouvee);
    app.post('*', HomeController.PageNonTrouvee);

};
