# L'application

L'application est décomposée en deux parties : 

- Espace client 
- Espace administration

Un dossier "Public" à la racine contient l'ensemble des éléments que les deux applications vont utiliser **mutuellement** (CSS, JS, photos et ressources)

Afin de pouvoir utiliser les deux applications en même temps, j'ai changé le **nom du cookie** pour la partie Administration en "GrandPrixAdmin". Sans cela, l'utilisateur était déconnecté dès qu'il actualisait une page client.
Les adresses

La partie **administration** utilise le **port 6800**, la partie **client** le **port 6900**.

Pour l'upload de fichier, la barre de chargement ne s'affichera probablement pas en local (trop rapide). Vous pouvez utiliser mon Raspberry afin de la tester (étant donné ma connexion internet des années 30) aux adresses suivante (si le serveur ne répond pas, prévenez-moi, je le rebooterai) :

- 78.214.72.153:6800
- 78.214.72.153:6900

(PS : La création de socket peut ne pas s'effectuer correctement sous des anciennes versions de navigateurs)


Bonnes corrections !