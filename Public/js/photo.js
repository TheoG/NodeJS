function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#rendu').attr('src', e.target.result);
            $('#rendu').css('display', 'block');
            $('#rendu').css('width', '10rem');
        }

        reader.readAsDataURL(input.files[0]);
    }
}